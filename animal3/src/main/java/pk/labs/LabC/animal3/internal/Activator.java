package pk.labs.LabC.animal3.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.logger.Logger;

public class Activator implements BundleActivator {

    static ServiceRegistration serv;
    
    @Override
    public void start(BundleContext context) throws Exception {
        Leniwiec.instance = new Leniwiec();
        Logger.get().log(this, "Leniwiec - ON");
        Activator.serv = context.registerService(Animal.class.getName(), Leniwiec.instance, null);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        Logger.get().log(this, "Leniwiec - OFF");
        Leniwiec.instance = null;
        serv.unregister();
    }
}