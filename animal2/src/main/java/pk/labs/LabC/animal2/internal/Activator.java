package pk.labs.LabC.animal2.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.logger.Logger;

public class Activator implements BundleActivator {

    static ServiceRegistration serv;
    
    @Override
    public void start(BundleContext context) throws Exception {
        Lis.instance = new Lis();
        Logger.get().log(this, "Lis - ON");
        Activator.serv = context.registerService(Animal.class.getName(), Lis.instance, null);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        Logger.get().log(this, "Lis - OFF");
        Lis.instance = null;
        serv.unregister();
    }
}
