package pk.labs.LabC.actions.internal;

import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.contracts.AnimalAction;

public class Picie implements AnimalAction {

    public static Picie instance;
    private final String name;

    public Picie() {
        name = "Picie";
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean execute(Animal animal) {
        animal.setStatus("Pije");
        return true;
    }

}
