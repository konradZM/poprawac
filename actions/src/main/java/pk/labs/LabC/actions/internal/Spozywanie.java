package pk.labs.LabC.actions.internal;

import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.contracts.AnimalAction;

public class Spozywanie implements AnimalAction {

    public static Spozywanie instance;
    private final String name;

    public Spozywanie() {
        name = "Spozywanie";
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean execute(Animal animal) {
        animal.setStatus("Pozywia sie");
        return true;
    }
}

