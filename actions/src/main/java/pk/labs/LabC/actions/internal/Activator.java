package pk.labs.LabC.actions.internal;

import java.util.Hashtable;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.AnimalAction;

public class Activator implements BundleActivator {

    private static BundleContext context;

    static BundleContext getContext() {
        return context;
    }

    @Override
    public void start(BundleContext context) throws Exception {
        Activator.context = context;

        Spanie.instance = new Spanie();
        Picie.instance = new Picie();
        Spozywanie.instance = new Spozywanie();

        Hashtable slonFilter = new Hashtable();
        Hashtable tygrysFilter = new Hashtable();
        Hashtable delfinFilter = new Hashtable();

        slonFilter.put("species", "Leniwiec");
        tygrysFilter.put("species", "Lew");
        delfinFilter.put("species", "Lis");
        
        slonFilter.put("name", "Spanie");
        tygrysFilter.put("name", "Ryczenie");
        delfinFilter.put("name", "Spozywanie");

        context.registerService(AnimalAction.class.getName(), Spanie.instance, slonFilter);
        context.registerService(AnimalAction.class.getName(), Picie.instance, tygrysFilter);
        context.registerService(AnimalAction.class.getName(), Spozywanie.instance, delfinFilter);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        Spanie.instance = null;
        Picie.instance = null;
        Spozywanie.instance = null;
    }
}
