package pk.labs.LabC.animal1.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.logger.Logger;

public class Activator implements BundleActivator {

    static ServiceRegistration serv;
    
    @Override
    public void start(BundleContext context) throws Exception {
        Lew.instance = new Lew();
        Logger.get().log(this, "Lew - ON");
        Activator.serv = context.registerService(Animal.class.getName(), Lew.instance, null);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        Logger.get().log(this, "Lew - OFF");
        Lew.instance = null;
        serv.unregister();
    }
}
